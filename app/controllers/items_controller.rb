class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy, :reload]

  # GET /items
  # GET /items.json
  def index
   
    @q = Item.ransack(params[:q])
    if params[:category_id] 
      @q.category_id_eq = params[:category_id]
    end
    @items = @q.result
    
    #@items = Item.all
  
  end
 
  def wrong_balance
    @items = Item.all.select { |m| m.balance != 0 } 
  end

  def danger
    @items = Item.all.select { |m| m.amount < m.red_line } 
    
    #@items = Item.all
  
  end


  # GET /items/1
  # GET /items/1.json
  def show
    @total_out = @item.transactions.where( :action_type => "Out").sum(:amount)
    @total_in = @item.transactions.where( :action_type => "In").sum(:amount)
    @transactions = @item.transactions.page(params[:page])
  end

  # GET /items/new
  def new
    @item = Item.new
    @category = Category.find(params[:category_id]) 
    @item.category = @category 
  end

  # GET /items/1/edit
  def edit
  end
  
  def reload
    @item.transactions.destroy_all
    Transaction.create(
      :item_id => @item.id, :action_type => "In", :user_id => current_user.id, :amount => @item.amount)
    
    respond_to do |format|
      format.html { redirect_to @item, notice: 'Обнуление выполнено' }
    end
  end
  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)
    @category = @item.category
    respond_to do |format|
      if @item.save
        Transaction.create(
          :item_id => @item.id, :action_type => "In", :user_id => current_user.id, :amount => @item.amount)
        format.html { redirect_to @category, notice: 'Item was successfully created.' }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.with_deleted.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:name, :sku, :red_line, :unit_type, :description, :price, :amount, :category_id)
    end
end
